package controller;

import model.Node;

import java.util.Map;

public interface Functionality <K extends Comparable<K>,V>{
    V get(Object key);
    Node<K,V> getNode(Object key);
    V put(K key,V value);
    V remove(K key);
    void putAll(Map<K,V> map);
    void print();
}
