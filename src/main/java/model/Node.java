package model;

public class Node<K extends Comparable<K>,V> {
    private K key;
    private V value;
    private Node <K,V> leftNode;
    private Node <K,V> rightNode;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public boolean hasLeft(){
        return leftNode!=null;
    }
    public boolean hasRight(){
        return  rightNode!=null;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public Node<K, V> getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node<K, V> leftNode) {
        this.leftNode = leftNode;
    }

    public Node<K, V> getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node<K, V> rightNode) {
        this.rightNode = rightNode;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;

        Node<?, ?> node = (Node<?, ?>) o;

        if (getKey() != null ? !getKey().equals(node.getKey()) : node.getKey() != null) return false;
        if (getValue() != null ? !getValue().equals(node.getValue()) : node.getValue() != null) return false;
        if (getLeftNode() != null ? !getLeftNode().equals(node.getLeftNode()) : node.getLeftNode() != null)
            return false;
        return getRightNode() != null ? getRightNode().equals(node.getRightNode()) : node.getRightNode() == null;
    }

    @Override
    public int hashCode() {
        int result = getKey() != null ? getKey().hashCode() : 0;
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + (getLeftNode() != null ? getLeftNode().hashCode() : 0);
        result = 31 * result + (getRightNode() != null ? getRightNode().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
