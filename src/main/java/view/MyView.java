package view;

import controller.Controller;
import model.MyTreeMap;
import model.Node;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private Map<Integer,Integer> map = new MyTreeMap();
    private Controller<Integer,Integer> controller= new Controller();

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Generate new MyTreeMap<Integer,Integer>");
        menu.put("2", "  2 - Input key - get value!!! ");
        menu.put("3", "  3 - Input key - get Node");
        menu.put("4", "  4 - Put your value in the map");
        menu.put("5", "  5 - Tree show");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        MyTreeMap map = new MyTreeMap<Integer,Integer>();
        for (int i=0;i<20;i++) {
            Double key = Math.random() * 100;
            Double value = Math.random() * 500;
            map.put(key.intValue(), value.intValue());
        }
        controller.putAll(map);

    }

    private void pressButton2() {
            System.out.println("Input key");
            Integer key = input.nextInt();
            System.out.println(controller.get(key).toString());
    }

    private void pressButton3() {
        System.out.println("Input key");
        Integer key = input.nextInt();
        System.out.println(controller.getNode(key).toString());
    }

    private void pressButton4() {
        System.out.println("Input key");
        Integer key = input.nextInt();
        System.out.println("Input value");
        Integer value = input.nextInt();
        controller.put(key,value);
    }

    private void pressButton5() {
    controller.print();

    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
